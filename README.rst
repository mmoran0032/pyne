pyne - Python for Nuclear Experiments
=====================================

`.evt` file processing based on [evt2root](https://github.com/ksmith0/evt2root)
by Karl Smith.


pyne.Experiment
---------------

Collector for all experimental raw data files, and primary experiment
organizer. You can specify the location of your data files, filter out any runs
you don't want to keep, organize runs into different user-defined classes, and
other organizational things.
